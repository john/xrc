" John DeSalvo
let g:colors_name="based"
"hi clear

hi Search cterm=bold ctermfg=15 ctermbg=1
hi Link cterm=bold ctermfg=215
hi StatusLine cterm=bold ctermfg=0 ctermbg=13
hi LineNr cterm=bold ctermfg=0 ctermbg=13
hi CursorLineNr cterm=bold ctermfg=13 ctermbg=0
hi CursorLine cterm=NONE ctermbg=88
hi CursorColumn cterm=NONE ctermbg=235
hi ErrorMsg ctermfg=0 ctermbg=1 
hi WarningMsg ctermfg=0 ctermbg=1
hi ModeMsg cterm=bold ctermfg=0 ctermbg=48
hi MatchParen ctermbg=55
hi Comment ctermfg=55 ctermbg=225
hi Constant ctermfg=14
hi Number ctermfg=27
hi Boolean ctermfg=27
hi Statement cterm=bold ctermfg=226
hi PreProc cterm=underline ctermfg=209
hi Identifier cterm=bold ctermfg=10
hi Type cterm=bold ctermfg=202
hi Special cterm=bold ctermfg=13
